-- the squid  controller
local mymodule = {}

mymodule.default_action = "status"

mymodule.status = function( self )
	return self.model.get_status()
end

mymodule.startstop = function( self )
	return self.handle_form(self, self.model.get_startstop, self.model.startstop_service, self.clientdata)
end

mymodule.listfiles = function( self )
	return self.model.list_files()
end

mymodule.edit = function( self )
	return self.handle_form(self, self.model.get_file, self.model.update_file, self.clientdata, "Save", "Edit File", "File Saved")
end

return mymodule
