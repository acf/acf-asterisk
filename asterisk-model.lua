-- Copyright(c) 2007 A. Brodmann - Licensed under terms of GPL2
local mymodule = {}

-- Load libraries
modelfunctions = require("modelfunctions")
posix = require("posix")
fs = require("acf.fs")
format = require("acf.format")
validator = require("acf.validator")

-- Set variables
local processname = "asterisk"
local packagename = "asterisk"
local baseurl = "/etc/asterisk"

-- ################################################################################
-- LOCAL FUNCTIONS

local is_valid_filename = function(filename)
	local dirname = posix.dirname(filename)
	return validator.is_valid_filename(filename) and string.match(dirname, baseurl) and not string.match(dirname, "%.%.")
end

-- ################################################################################
-- PUBLIC FUNCTIONS

mymodule.get_status = function()
	return modelfunctions.getstatus(processname, packagename, "Asterisk Status")
end

mymodule.get_startstop = function(self, clientdata)
	return modelfunctions.get_startstop(processname)
end

mymodule.startstop_service = function(self, startstop, action)
	return modelfunctions.startstop_service(startstop, action)
end

mymodule.get_file = function(self, clientdata)
	return modelfunctions.getfiledetails(clientdata.filename, is_valid_filename)
end

mymodule.update_file = function(self, filedetails)
	local ret = modelfunctions.setfiledetails(self, filedetails, is_valid_filename)
	if not ret.errtxt then
		posix.chmod(filedetails.value.filename.value, "rw-------")
		posix.chown(filedetails.value.filename.value, posix.getpasswd("asterisk", "uid") or 0, posix.getpasswd("asterisk", "gid") or 0)
	end
	return ret
end

mymodule.list_files = function()
	local retval = {}
	for file in fs.find(null, baseurl) do
		local details = posix.stat(file)
		if details.type == "regular" then
			details.filename = file
			table.insert(retval, details)
		end
	end
	table.sort(retval, function(a,b) return a.filename < b.filename end)
	return cfe({ type="structure", value=retval, label="List of Asterisk files" })
end

return mymodule
